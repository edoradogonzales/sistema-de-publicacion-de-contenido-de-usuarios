import {DefaultCrudRepository, repository, HasManyThroughRepositoryFactory} from '@loopback/repository';
import {Posts, PostsRelations, Tag, DetailPostsTag} from '../models';
import {DbContenidoUsuarioDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {DetailPostsTagRepository} from './detail-posts-tag.repository';
import {TagRepository} from './tag.repository';

export class PostsRepository extends DefaultCrudRepository<
  Posts,
  typeof Posts.prototype.id_posts,
  PostsRelations
> {

  public readonly tags: HasManyThroughRepositoryFactory<Tag, typeof Tag.prototype.id_tag,
          DetailPostsTag,
          typeof Posts.prototype.id_posts
        >;

  constructor(
    @inject('datasources.db_contenido_usuario') dataSource: DbContenidoUsuarioDataSource, @repository.getter('DetailPostsTagRepository') protected detailPostsTagRepositoryGetter: Getter<DetailPostsTagRepository>, @repository.getter('TagRepository') protected tagRepositoryGetter: Getter<TagRepository>,
  ) {
    super(Posts, dataSource);
    this.tags = this.createHasManyThroughRepositoryFactoryFor('tags', tagRepositoryGetter, detailPostsTagRepositoryGetter,);
    this.registerInclusionResolver('tags', this.tags.inclusionResolver);
  }
}
