import {DefaultCrudRepository} from '@loopback/repository';
import {DetailPostsTag, DetailPostsTagRelations} from '../models';
import {DbContenidoUsuarioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class DetailPostsTagRepository extends DefaultCrudRepository<
  DetailPostsTag,
  typeof DetailPostsTag.prototype.id_detail,
  DetailPostsTagRelations
> {
  constructor(
    @inject('datasources.db_contenido_usuario') dataSource: DbContenidoUsuarioDataSource,
  ) {
    super(DetailPostsTag, dataSource);
  }
}
