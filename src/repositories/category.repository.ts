import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {Category, CategoryRelations, Posts} from '../models';
import {DbContenidoUsuarioDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PostsRepository} from './posts.repository';

export class CategoryRepository extends DefaultCrudRepository<
  Category,
  typeof Category.prototype.id_category,
  CategoryRelations
> {

  public readonly posts: HasManyRepositoryFactory<Posts, typeof Category.prototype.id_category>;

  constructor(
    @inject('datasources.db_contenido_usuario') dataSource: DbContenidoUsuarioDataSource, @repository.getter('PostsRepository') protected postsRepositoryGetter: Getter<PostsRepository>,
  ) {
    super(Category, dataSource);
    this.posts = this.createHasManyRepositoryFactoryFor('posts', postsRepositoryGetter,);
    this.registerInclusionResolver('posts', this.posts.inclusionResolver);
  }
}
