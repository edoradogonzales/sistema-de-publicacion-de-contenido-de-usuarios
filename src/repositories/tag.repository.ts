import {DefaultCrudRepository} from '@loopback/repository';
import {Tag, TagRelations} from '../models';
import {DbContenidoUsuarioDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TagRepository extends DefaultCrudRepository<
  Tag,
  typeof Tag.prototype.id_tag,
  TagRelations
> {
  constructor(
    @inject('datasources.db_contenido_usuario') dataSource: DbContenidoUsuarioDataSource,
  ) {
    super(Tag, dataSource);
  }
}
