import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {User, UserRelations, Posts} from '../models';
import {DbContenidoUsuarioDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {PostsRepository} from './posts.repository';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id_User,
  UserRelations
> {

  public readonly posts: HasManyRepositoryFactory<Posts, typeof User.prototype.id_User>;

  constructor(
    @inject('datasources.db_contenido_usuario') dataSource: DbContenidoUsuarioDataSource, @repository.getter('PostsRepository') protected postsRepositoryGetter: Getter<PostsRepository>,
  ) {
    super(User, dataSource);
    this.posts = this.createHasManyRepositoryFactoryFor('posts', postsRepositoryGetter,);
    this.registerInclusionResolver('posts', this.posts.inclusionResolver);
  }
}
