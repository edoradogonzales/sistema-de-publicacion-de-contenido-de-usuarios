import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Category,
  Posts,
} from '../models';
import {CategoryRepository} from '../repositories';

export class CategoryPostsController {
  constructor(
    @repository(CategoryRepository) protected categoryRepository: CategoryRepository,
  ) { }

  @get('/categories/{id}/posts', {
    responses: {
      '200': {
        description: 'Array of Category has many Posts',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Posts)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Posts>,
  ): Promise<Posts[]> {
    return this.categoryRepository.posts(id).find(filter);
  }

  @post('/categories/{id}/posts', {
    responses: {
      '200': {
        description: 'Category model instance',
        content: {'application/json': {schema: getModelSchemaRef(Posts)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Category.prototype.id_category,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Posts, {
            title: 'NewPostsInCategory',
            exclude: ['id_posts'],
            optional: ['categoryId']
          }),
        },
      },
    }) posts: Omit<Posts, 'id_posts'>,
  ): Promise<Posts> {
    return this.categoryRepository.posts(id).create(posts);
  }

  @patch('/categories/{id}/posts', {
    responses: {
      '200': {
        description: 'Category.Posts PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Posts, {partial: true}),
        },
      },
    })
    posts: Partial<Posts>,
    @param.query.object('where', getWhereSchemaFor(Posts)) where?: Where<Posts>,
  ): Promise<Count> {
    return this.categoryRepository.posts(id).patch(posts, where);
  }

  @del('/categories/{id}/posts', {
    responses: {
      '200': {
        description: 'Category.Posts DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Posts)) where?: Where<Posts>,
  ): Promise<Count> {
    return this.categoryRepository.posts(id).delete(where);
  }
}
