import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
  import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
Posts,
DetailPostsTag,
Tag,
} from '../models';
import {PostsRepository} from '../repositories';

export class PostsTagController {
  constructor(
    @repository(PostsRepository) protected postsRepository: PostsRepository,
  ) { }

  @get('/posts/{id}/tags', {
    responses: {
      '200': {
        description: 'Array of Posts has many Tag through DetailPostsTag',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Tag)},
          },
        },
      },
    },
  })
  async find(
    @param.path.number('id') id: number,
    @param.query.object('filter') filter?: Filter<Tag>,
  ): Promise<Tag[]> {
    return this.postsRepository.tags(id).find(filter);
  }

  @post('/posts/{id}/tags', {
    responses: {
      '200': {
        description: 'create a Tag model instance',
        content: {'application/json': {schema: getModelSchemaRef(Tag)}},
      },
    },
  })
  async create(
    @param.path.number('id') id: typeof Posts.prototype.id_posts,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tag, {
            title: 'NewTagInPosts',
            exclude: ['id_tag'],
          }),
        },
      },
    }) tag: Omit<Tag, 'id_tag'>,
  ): Promise<Tag> {
    return this.postsRepository.tags(id).create(tag);
  }

  @patch('/posts/{id}/tags', {
    responses: {
      '200': {
        description: 'Posts.Tag PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tag, {partial: true}),
        },
      },
    })
    tag: Partial<Tag>,
    @param.query.object('where', getWhereSchemaFor(Tag)) where?: Where<Tag>,
  ): Promise<Count> {
    return this.postsRepository.tags(id).patch(tag, where);
  }

  @del('/posts/{id}/tags', {
    responses: {
      '200': {
        description: 'Posts.Tag DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.number('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Tag)) where?: Where<Tag>,
  ): Promise<Count> {
    return this.postsRepository.tags(id).delete(where);
  }
}
