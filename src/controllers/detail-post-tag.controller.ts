import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {DetailPostsTag} from '../models';
import {DetailPostsTagRepository} from '../repositories';

export class DetailPostTagController {
  constructor(
    @repository(DetailPostsTagRepository)
    public detailPostsTagRepository : DetailPostsTagRepository,
  ) {}

  @post('/detail-posts-tags', {
    responses: {
      '200': {
        description: 'DetailPostsTag model instance',
        content: {'application/json': {schema: getModelSchemaRef(DetailPostsTag)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DetailPostsTag, {
            title: 'NewDetailPostsTag',
            exclude: ['id_detail'],
          }),
        },
      },
    })
    detailPostsTag: Omit<DetailPostsTag, 'id_detail'>,
  ): Promise<DetailPostsTag> {
    return this.detailPostsTagRepository.create(detailPostsTag);
  }

  @get('/detail-posts-tags/count', {
    responses: {
      '200': {
        description: 'DetailPostsTag model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(DetailPostsTag) where?: Where<DetailPostsTag>,
  ): Promise<Count> {
    return this.detailPostsTagRepository.count(where);
  }

  @get('/detail-posts-tags', {
    responses: {
      '200': {
        description: 'Array of DetailPostsTag model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(DetailPostsTag, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(DetailPostsTag) filter?: Filter<DetailPostsTag>,
  ): Promise<DetailPostsTag[]> {
    return this.detailPostsTagRepository.find(filter);
  }

  @patch('/detail-posts-tags', {
    responses: {
      '200': {
        description: 'DetailPostsTag PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DetailPostsTag, {partial: true}),
        },
      },
    })
    detailPostsTag: DetailPostsTag,
    @param.where(DetailPostsTag) where?: Where<DetailPostsTag>,
  ): Promise<Count> {
    return this.detailPostsTagRepository.updateAll(detailPostsTag, where);
  }

  @get('/detail-posts-tags/{id}', {
    responses: {
      '200': {
        description: 'DetailPostsTag model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(DetailPostsTag, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(DetailPostsTag, {exclude: 'where'}) filter?: FilterExcludingWhere<DetailPostsTag>
  ): Promise<DetailPostsTag> {
    return this.detailPostsTagRepository.findById(id, filter);
  }

  @patch('/detail-posts-tags/{id}', {
    responses: {
      '204': {
        description: 'DetailPostsTag PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DetailPostsTag, {partial: true}),
        },
      },
    })
    detailPostsTag: DetailPostsTag,
  ): Promise<void> {
    await this.detailPostsTagRepository.updateById(id, detailPostsTag);
  }

  @put('/detail-posts-tags/{id}', {
    responses: {
      '204': {
        description: 'DetailPostsTag PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() detailPostsTag: DetailPostsTag,
  ): Promise<void> {
    await this.detailPostsTagRepository.replaceById(id, detailPostsTag);
  }

  @del('/detail-posts-tags/{id}', {
    responses: {
      '204': {
        description: 'DetailPostsTag DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.detailPostsTagRepository.deleteById(id);
  }
}
