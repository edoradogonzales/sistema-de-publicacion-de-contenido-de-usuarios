import {Entity, model, property} from '@loopback/repository';

@model()
export class DetailPostsTag extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id_detail?: number;



  @property({
    type: 'number',
  })
  postsId?: number;

  @property({
    type: 'number',
  })
  tagId?: number;

  constructor(data?: Partial<DetailPostsTag>) {
    super(data);
  }
}

export interface DetailPostsTagRelations {
  // describe navigational properties here
}

export type DetailPostsTagWithRelations = DetailPostsTag & DetailPostsTagRelations;
