import {Entity, model, property, hasMany} from '@loopback/repository';
import {Tag} from './tag.model';
import {DetailPostsTag} from './detail-posts-tag.model';

@model()
export class Posts extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id_posts?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  content: string;

  @property({
    type: 'string',
  })
  file?: string;

  @property({
    type: 'date',
    required: true,
  })
  date: string;

  @property({
    type: 'number',
  })
  userId?: number;

  @property({
    type: 'number',
  })
  categoryId?: number;

  @hasMany(() => Tag, {through: {model: () => DetailPostsTag}})
  tags: Tag[];

  constructor(data?: Partial<Posts>) {
    super(data);
  }
}

export interface PostsRelations {
  // describe navigational properties here
}

export type PostsWithRelations = Posts & PostsRelations;
