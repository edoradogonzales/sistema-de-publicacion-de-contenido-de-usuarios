import {Entity, model, property, hasMany} from '@loopback/repository';
import {Posts} from './posts.model';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id_User?: number;

  @property({
    type: 'date',
    required: true,
  })
  birth_date: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'string',
  })
  card_identity?: string;

  @property({
    type: 'boolean',
  })
  sex?: boolean;

  @property({
    type: 'string',
  })
  phone?: string;

  @hasMany(() => Posts)
  posts: Posts[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
